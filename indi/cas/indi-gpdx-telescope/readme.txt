driver_gpxd.h : Protocoll Arduino - General - INDI

gpdxdriver.h / gpdxdriver.cpp - Zwischenschicht für eigenen Treiber oder INDI-Treiber.
Vermittelt zwischen Arduino und INDI.

gpdxscope.h /gpdxscope.cpp  - INDI Treiber



[compile]
i) qtcreator (siehe indi website)
ii)  cd build/
iii)  cmake -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_BUILD_TYPE=Debug /home/torsten/devel/projects/indi/astronomy/indi/cas/indi-gpdx-telescope/
iv) make


[run]
i) goto build dir
ii) indiserver -v ./indi_gpdx_telescope 
iii) start INDIStarter for client test


[snippet]
    //   Coordinates double to string
    //   char RAStr[16], DecStr[16];
    //   fs_sexa(RAStr, RA, 2, 3600);
    //   fs_sexa(DecStr, DE, 2, 3600);
    //   LOGF_DEBUG("Goto RA-DEC(%s,%s)", RAStr, DecStr);

    //   Coordinates string to double
    //   double ra,dec;
    //   f_scansexa(RAStr,&ra);
    //   f_scansexa(DecStr,&dec);
    //   LOGF_DEBUG("Goto RA-DEC(%g,%g)", ra,dec);


[install]
copy to:
/usr/bin
driver bekannt machen
/usr/share/indi/driver.xml 
