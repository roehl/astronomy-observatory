#pragma once

#ifndef GPDX_SCOPE_H
#define GPDX_SCOPE_H_H

#include "driver_config.h"
#include "gpdxdriver.h"

#include "indiguiderinterface.h"
#include "inditelescope.h"

// libnova needed for equatorialToHorizontal()
#include <libnova/transform.h>
#include <libnova/precession.h>
// libnova specifies round() on old systems and it collides with the new gcc 5.x/6.x headers
#define HAVE_ROUND
#include <libnova/utility.h>


class GPDXScope : public INDI::Telescope, public INDI::GuiderInterface
{
public:
    GPDXScope();

    virtual const char *getDefaultName() override;
    virtual bool initProperties() override;
    virtual bool updateProperties() override;
    virtual bool ISNewNumber(const char *dev, const char *name, double values[], char *names[], int n) override;

protected:

    ///////////////////////////////////////////////////////////////////////////////
    /// Communication Commands
    ///////////////////////////////////////////////////////////////////////////////   
    virtual bool Handshake() override;  
    virtual bool ReadScopeStatus() override;
    ///////////////////////////////////////////////////////////////////////////////
    /// Motions commands.
    ///////////////////////////////////////////////////////////////////////////////  
    virtual bool MoveNS(INDI_DIR_NS dir, TelescopeMotionCommand command) override;   
    virtual bool MoveWE(INDI_DIR_WE dir, TelescopeMotionCommand command) override;
    virtual bool SetSlewRate(int index) override;
    virtual bool Abort() override;
    ///////////////////////////////////////////////////////////////////////////////
    /// Pulse Guiding Commands
    ///////////////////////////////////////////////////////////////////////////////
    virtual IPState GuideNorth(uint32_t ms) override;
    virtual IPState GuideSouth(uint32_t ms) override;
    virtual IPState GuideEast(uint32_t ms) override;
    virtual IPState GuideWest(uint32_t ms) override;
    ///////////////////////////////////////////////////////////////////////////////
    /// Tracking Commands
    ///////////////////////////////////////////////////////////////////////////////
    virtual bool SetTrackMode(uint8_t mode) override;
    virtual bool SetTrackEnabled(bool enabled) override;
    virtual bool SetTrackRate(double raRate, double deRate) override;
    ///////////////////////////////////////////////////////////////////////////////
    /// GOTO & Sync commands
    ///////////////////////////////////////////////////////////////////////////////
    virtual bool Goto(double RA, double DE) override;
    virtual bool Sync(double RA, double DE) override;
    ///////////////////////////////////////////////////////////////////////////////
    /// Parking commands
    ///////////////////////////////////////////////////////////////////////////////
    virtual bool Park() override;
    virtual bool UnPark() override;
    virtual bool SetCurrentPark() override;
    virtual bool SetDefaultPark() override;

private:
    GPDXDriver driver;

    INumber GuideRateN[2];
    INumberVectorProperty GuideRateNP;
    // Parking Data
    ln_hrz_posn equatorialToHorizontal();
    double obs_latitude = 54;
    double obs_longitude = 10;
    double park_latitude = 54;
    double park_longitude = 359;
};

#endif // GPDX_SCOPE
