#include "indicom.h"

#include "gpdxscope.h"
#include "gpdxdriver.h"

#include <termios.h>
#include <cmath>
#include <cstring>
#include <memory>
#include <map>



// Single unique pointer to the driver.
static std::unique_ptr<GPDXScope> gpdxScope(new GPDXScope());

// Standard INDI API functions
void ISGetProperties(const char *dev)
{
    gpdxScope->ISGetProperties(dev);
}

void ISNewSwitch(const char *dev, const char *name, ISState *states, char *names[], int n)
{
    gpdxScope->ISNewSwitch(dev, name, states, names, n);
}

void ISNewText(const char *dev, const char *name, char *texts[], char *names[], int n)
{
    gpdxScope->ISNewText(dev, name, texts, names, n);
}

void ISNewNumber(const char *dev, const char *name, double values[], char *names[], int n)
{
    gpdxScope->ISNewNumber(dev, name, values, names, n);
}

void ISNewBLOB(const char *dev, const char *name, int sizes[], int blobsizes[], char *blobs[], char *formats[],
               char *names[], int n)
{
    INDI_UNUSED(dev);
    INDI_UNUSED(name);
    INDI_UNUSED(sizes);
    INDI_UNUSED(blobsizes);
    INDI_UNUSED(blobs);
    INDI_UNUSED(formats);
    INDI_UNUSED(names);
    INDI_UNUSED(n);
}
void ISSnoopDevice(XMLEle *root)
{
    gpdxScope->ISSnoopDevice(root);
}

GPDXScope::GPDXScope()
{
    setVersion(VERSION_MAJOR, VERSION_MINOR);

    SetTelescopeCapability(TELESCOPE_CAN_PARK | TELESCOPE_CAN_SYNC | TELESCOPE_CAN_GOTO | TELESCOPE_CAN_ABORT |
                           TELESCOPE_HAS_TRACK_MODE | TELESCOPE_CAN_CONTROL_TRACK | TELESCOPE_HAS_TRACK_RATE, 4);

}

const char *GPDXScope::getDefaultName()
{
    return GPDX_DEVICENAME;
}

bool GPDXScope::initProperties()
{
    // Make sure to init parent properties first
    INDI::Telescope::initProperties();

    // How fast do we guide compared to sidereal rate
    IUFillNumber(&GuideRateN[AXIS_RA], "GUIDE_RATE_WE", "W/E Rate", "%.1f", 0, 1, 0.1, 0.5);
    IUFillNumber(&GuideRateN[AXIS_DE], "GUIDE_RATE_NS", "N/S Rate", "%.1f", 0, 1, 0.1, 0.5);
    IUFillNumberVector(&GuideRateNP, GuideRateN, 2, getDeviceName(), "GUIDE_RATE", "Guiding Rate", MOTION_TAB, IP_RW, 0, IPS_IDLE);

    // Since we have 4 slew rates, let's fill them out
    IUFillSwitch(&SlewRateS[SLEW_GUIDE], "SLEW_GUIDE", "Guide", ISS_OFF);
    IUFillSwitch(&SlewRateS[SLEW_CENTERING], "SLEW_CENTERING", "Centering", ISS_OFF);
    IUFillSwitch(&SlewRateS[SLEW_FIND], "SLEW_FIND", "Find", ISS_OFF);
    IUFillSwitch(&SlewRateS[SLEW_MAX], "SLEW_MAX", "Max", ISS_ON);
    IUFillSwitchVector(&SlewRateSP, SlewRateS, 4, getDeviceName(), "TELESCOPE_SLEW_RATE", "Slew Rate", MOTION_TAB,
                       IP_RW, ISR_1OFMANY, 0, IPS_IDLE);

    // Add Tracking Modes. If you have SOLAR, LUNAR..etc, add them here as well.
    AddTrackMode("TRACK_SIDEREAL", "Sidereal", true);
    AddTrackMode("TRACK_SOLAR", "Solar");
    AddTrackMode("TRACK_LUNAR", "Lunar");
    AddTrackMode("TRACK_CUSTOM", "Custom");

    // The mount is initially in IDLE state.
    TrackState = SCOPE_IDLE;

    // How does the mount perform parking?
    SetParkDataType(PARK_AZ_ALT);
    // Let init the pulse guiding properties
    initGuiderProperties(getDeviceName(), MOTION_TAB);
    // Add debug controls
    addDebugControl();
    // Set the driver interface to indicate that we can also do pulse guiding
    setDriverInterface(getDriverInterface() | GUIDER_INTERFACE);
    // We want to query the mount every 900ms by default. The user can override this value.
    setDefaultPollingPeriod(900);

    return true;
}

bool GPDXScope::updateProperties()
{
    INDI::Telescope::updateProperties();

    if (isConnected())
    {
        defineNumber(&GuideNSNP);
        defineNumber(&GuideWENP);
        defineNumber(&GuideRateNP);

        // Read the parking file, and check if we can load any saved parking information.
        if (InitPark())
        {
            // If loading parking data is successful, we just set the default parking values.
            // By default in this example, we consider parking positoin Az=0 and Alt=0
            SetAxis1ParkDefault(park_longitude);
            SetAxis2ParkDefault(park_latitude);
        }
        else
        {
            // Otherwise, we set all parking data to default in case no parking data is found.
            SetAxis1Park(park_longitude);
            SetAxis2Park(park_latitude);
            SetAxis1ParkDefault(park_longitude);
            SetAxis2ParkDefault(park_latitude);
        }
    }
    else
    {
        deleteProperty(GuideNSNP.name);
        deleteProperty(GuideWENP.name);
        deleteProperty(GuideRateNP.name);
    }

    return true;
}

bool GPDXScope::Handshake()
{
    driver.setSerialPort(PortFD);  // once only!
    return driver.handshake();
}

bool GPDXScope::ReadScopeStatus()
{
    if(!driver.ReadScopeStatus())
        return false;

    double RA = driver.getRA();
    double DE = driver.getDEC();

    NewRaDec(RA, DE);

    return true;
}

bool GPDXScope::Goto(double RA, double DE)
{
    // step 0: set target
    if(!driver.Goto(RA,DE))
        return false;

    // step 1: stop motion
    if(!driver.abort())
        return false;

    // step 2: goto target
    if(!driver.move())
        return false;

    TrackState = SCOPE_SLEWING;

    return true;
}

bool GPDXScope::Sync(double RA, double DE)
{
    if(!driver.Sync(RA,DE))
        return false;

    NewRaDec(RA, DE);
    return true;
}

bool GPDXScope::SetSlewRate(int index){   
    return  driver.SetSlewRate(index);
}

bool GPDXScope::Park()
{
    // Send command for parking here
    TrackState = SCOPE_PARKING;
    LOG_INFO("Parking telescope in progress...");

    return true;
}

bool GPDXScope::UnPark()
{
    LOG_INFO(" GPDXScope::UnPark() called");
    SetParked(false);
    return true;
}

bool GPDXScope::ISNewNumber(const char *dev, const char *name, double values[], char *names[], int n)
{
    if (dev != nullptr && strcmp(dev, getDeviceName()) == 0)
    {
        // Guide Rate
        if (strcmp(name, "GUIDE_RATE") == 0)
        {
            IUUpdateNumber(&GuideRateNP, values, names, n);
            GuideRateNP.s = IPS_OK;
            IDSetNumber(&GuideRateNP, nullptr);
            return true;
        }

        // For guiding pulse, let's pass the properties up to the guide framework
        if (strcmp(name, GuideNSNP.name) == 0 || strcmp(name, GuideWENP.name) == 0)
        {
            processGuiderProperties(name, values, names, n);
            return true;
        }
    }

    // Otherwise, send it up the chains to INDI::Telescope to process any further properties
    return INDI::Telescope::ISNewNumber(dev, name, values, names, n);
}


bool GPDXScope::Abort()
{
    LOG_INFO(" GPDXScope::Abort called");
    return driver.abort();
}

bool GPDXScope::MoveNS(INDI_DIR_NS dir, TelescopeMotionCommand command)
{
    INDI_UNUSED(dir);
    INDI_UNUSED(command);
    if (TrackState == SCOPE_PARKED)
    {
        LOG_ERROR("Please unpark the mount before issuing any motion commands.");
        return false;
    }

    // Implement here the actual calls to do the motion requested
    GPDX_DIRECTION gpdxDir = (dir == DIRECTION_NORTH) ? GPDX_DIRECTION_NORTH : GPDX_DIRECTION_SOUTH;
    bool start = (command == MOTION_START) ? true:  false;

    return driver.MoveNS(gpdxDir,start);
}

bool GPDXScope::MoveWE(INDI_DIR_WE dir, TelescopeMotionCommand command)
{
    INDI_UNUSED(dir);
    INDI_UNUSED(command);
    if (TrackState == SCOPE_PARKED)
    {
        LOG_ERROR("Please unpark the mount before issuing any motion commands.");
        return false;
    }

    // Implement here the actual calls to do the motion requested
    GPDX_DIRECTION gpdxDir = (dir == DIRECTION_WEST) ? GPDX_DIRECTION_WEST : GPDX_DIRECTION_EAST;
    bool start = (command == MOTION_START) ? true:  false;

    return driver.MoveWE(gpdxDir,start);
}

IPState GPDXScope::GuideNorth(uint32_t ms)
{
    INDI_UNUSED(ms);
    // Implement here the actual calls to do the motion requested
    return IPS_BUSY;
}

IPState GPDXScope::GuideSouth(uint32_t ms)
{
    INDI_UNUSED(ms);
    // Implement here the actual calls to do the motion requested
    return IPS_BUSY;
}

IPState GPDXScope::GuideEast(uint32_t ms)
{
    INDI_UNUSED(ms);
    // Implement here the actual calls to do the motion requested
    return IPS_BUSY;
}

IPState GPDXScope::GuideWest(uint32_t ms)
{
    INDI_UNUSED(ms);
    // Implement here the actual calls to do the motion requested
    return IPS_BUSY;
}



bool GPDXScope::SetCurrentPark()
{   
    // Assumg PARK_AZ_ALT, we need to do something like this:

    ln_hrz_posn AltAZ = equatorialToHorizontal();
    SetAxis1Park(AltAZ.az);
    SetAxis2Park(AltAZ.alt);

    return true;
}


ln_hrz_posn GPDXScope::equatorialToHorizontal()
{
    ln_equ_posn Eq { 0, 0 };
    ln_hrz_posn AltAz { 0, 0 };

    // Set the current location
    double ra = driver.getRA();
    double dec = driver.getDEC();

    ln_lnlat_posn  loc;
    loc.lat = obs_latitude;
    loc.lng = obs_longitude;

    Eq.ra  = ra * 360.0 / 24.0;
    Eq.dec = dec;

    ln_get_hrz_from_equ(&Eq, &loc, ln_get_julian_from_sys(), &AltAz);
    AltAz.az -= 180;

    if (AltAz.az < 0)
        AltAz.az += 360;

    return AltAz;
}


bool GPDXScope::SetDefaultPark()
{
    // By default az to north, and alt to pole
    LOG_DEBUG("Setting Park Data to Default.");
    SetAxis1Park(park_longitude);
    SetAxis2Park(park_latitude);

    return true;
}

bool GPDXScope::SetTrackMode(uint8_t mode)
{
    // Sidereal/Lunar/Solar/Custom
    return  driver.setTrackMode(mode);
}

bool GPDXScope::SetTrackEnabled(bool enabled)
{
    return driver.setTrackEnabled(enabled);
}

bool GPDXScope::SetTrackRate(double raRate, double deRate)
{   
    return driver.setTrackRate(raRate,deRate);
}
