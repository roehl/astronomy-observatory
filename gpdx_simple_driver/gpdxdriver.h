#ifndef GPDXDRIVER_H
#define GPDXDRIVER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>

#include "driver_gpdx.h"



class GPDXDriver
{

public:
    GPDXDriver();
    ~GPDXDriver();

    bool driverOpen(char* device);
    bool driverClose();

    bool sendMessage(const byte command, char* body);
    bool readMessage();
    char* getMessage();



private:
    bool serialOpen(char* device);
    bool serialWrite(char* buf, int num);
    bool serialRead(char* buf, int num);
    bool serialClose();

private:
    char response[CMD_MAX_LEN];  // after every read response contains the answer
    int m_fd;                    // file descriptor
    int m_delay;                 // delay after read/write operations
    struct termios m_options_old;

};

#endif // GPDXDRIVER_H
