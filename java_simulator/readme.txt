[1 virtuelle serielle schnittstelle erzeugen]
socat -d -d PTY,link=/dev/ttyUSB60 PTY,link=/dev/ttyUSB61

[2 rechte setzen]
# chmod 777 /dev/ttyUSB6*

[3 testen]
i) auf der konsole mit:echo ":GD#" > /dev/ttyUSB60
test01.java sollte nachricht anzeigen

ii) mit stellarium
lx200 teleskop einrichten mit /dev/ttyUSB60 als device

[4] schnittstelle muss häufig neueingerichtet werden (schritte 1 + 2 )
