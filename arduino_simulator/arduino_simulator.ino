

/*
 * driver demo Stellarium-Arduino
 */
 
byte buffer[1];
char input[20];
byte currentRA[9];
byte currentDEC[10];
 
 
/* serial communication protocol lx200 */
byte cmd;
const byte PROTOCOL_GR =  54; // :GR#   get RA
const byte PROTOCOL_GD =  55; // :GD#   get DEC
const byte PROTOCOL_Q =  56;  // :Q# Bewegung stoppen
const byte PROTOCOL_SR =  57; // setze RA FORMAT  :Sr HH:MM:SS#
const byte PROTOCOL_SD =  58; // setze DEC FORMAT :Sd sDD*MM:SS#
const byte PROTOCOL_MS =  59; // :MS# Bewegung zum Ziel starten
 
void setup()
{
  Serial.begin(9600);
  //set currentRA = "02:31:50#";
  currentRA[0] = 48; currentRA[1] = 50; currentRA[2] = 58; currentRA[3] = 51;
  currentRA[4] = 49; currentRA[5] = 58; currentRA[6] = 53; currentRA[7] = 48; currentRA[8] = 35;
  // set currentDE = "+89*15:00#";
  currentDEC[0] = 43;  currentDEC[1] = 56;  currentDEC[2] = 57;  currentDEC[3] = 42;  currentDEC[4] = 49;
  currentDEC[5] = 53;  currentDEC[6] = 58;  currentDEC[7] = 48;  currentDEC[8] = 48;  currentDEC[9] = 35;
}
 
void loop()
{
 
  if (Serial.available() > 0) {
 
    cmd =  read();
 
    switch (cmd) {
 
      case PROTOCOL_GR:
        send(currentRA, 9);
        break;
 
      case PROTOCOL_GD:
        send(currentDEC, 10);
        break;
 
      case PROTOCOL_Q:
        // do nothing!
        break;
 
      case PROTOCOL_SR:
        // RA empfangen bestätigen mit 1
        buffer[0] = 49;
        send(buffer, 1);
        break;
 
      case PROTOCOL_SD:
        // DEC empfangen bestätigen mit 1
        buffer[0] = 49;
        send(buffer, 1);
        break;
 
      case PROTOCOL_MS:
        // bestätigen mit 0
        buffer[0] = 48;
        send(buffer, 1);
        break;
    }
 
  }
 
}
 
 
byte read() {
 
  int i = 0;
  input[i++] = Serial.read();
  delay(5);
 
  while ((input[i++] = Serial.read()) != '#') {
    delay(5);
  }
  delay(100);
  input[i] = '\0';
 
  if (input[1] == ':' && input[2] == 'G' && input[3] == 'R' && input[4] == '#') {
    return PROTOCOL_GR;
  }
 
  if (input[1] == ':' && input[2] == 'G' && input[3] == 'D' && input[4] == '#') {
    return PROTOCOL_GD;
  }
 
  if (input[1] == ':' && input[2] == 'Q' && input[3] == '#') {
    return PROTOCOL_Q;
  }
 
  if (input[0] == ':' && input[1] == 'S' && input[2] == 'r') {
    // RA empfangen :Sr HH:MM:SS#
    // Leerzeichen fehlt im LX-Protokoll!
    currentRA[0] = input[4];
    currentRA[1] = input[5];
    currentRA[2] = 58;
    currentRA[3] = input[7];
    currentRA[4] = input[8];
    currentRA[5] = 58;
    currentRA[6] = input[10];
    currentRA[7] = input[11];
    currentRA[8] = 35;
 
    return PROTOCOL_SR;
  }
 
  if (input[0] == ':' && input[1] == 'S' && input[2] == 'd') {
    // DEC empfangen :Sd sDD*MM#
    //Leerzeichen fehlt im LX-Protokoll!
    currentDEC[0] = input[4];
    currentDEC[1] = input[5];
    currentDEC[2] = input[6];
    currentDEC[3] = 42 ;
    currentDEC[4] = input[8];
    currentDEC[5] = input[9];
    currentDEC[6] = 58;
    currentDEC[7] = 48;
    currentDEC[8] = 48;
    currentDEC[9] = 35;
 
 
    return PROTOCOL_SD;
  }
 
  if (input[0] == ':' && input[1] == 'M' && input[2] == 'S' && input[3] == '#') {
    return PROTOCOL_MS;
  }
 
  return -1;
}
 
void send(byte lsData[], int len) {
 
  for (int i = 0; i < len; i++ ) {
    Serial.write(lsData[i]);
  }
 
}


