

#include "gpdxscope.h"
#include "gpdxdriver.h"
#include "driver_config.h"

#include "indilogger.h"
#include "indicom.h"

#include <map>
#include <cstring>
#include <cmath>
#include <termios.h>
#include <unistd.h>
#include <memory>
#include <iostream>
#include <string>
using namespace std;

GPDXDriver::GPDXDriver()
{

}

// This method is required by the logging macros
const char *GPDXDriver::getDeviceName()
{
    return GPDX_DEVICENAME;
}
bool GPDXDriver::isOK(){

    if( response[0] == CMD_SEND_RECV_OK[0] &&
            response[1] == CMD_SEND_RECV_OK[1] &&
            response[2] == CMD_SEND_RECV_OK[2])
        return true;

    return false;
}

bool GPDXDriver::handshake(){

    if(!sendCommand(CMD_SEND_ACK))
        return false;

    if( response[0]== CMD_RECV_ACK[0] &&
            response[1]== CMD_RECV_ACK[1] &&
            response[2]== CMD_RECV_ACK[2])
        return true;

    return false;
}
bool GPDXDriver::ReadScopeStatus(){


    if(!sendCommand(CMD_RECV_STA))
        return false;

    LOGF_DEBUG("ReadScopeStatus: <%s>" , response);

    // feed info -  info available via getterMethod
    sscanf(response, "[%lf:%lf]", &m_ra, &m_dec);


    return true;
}

double GPDXDriver::getRA(){
    return m_ra;
}

double GPDXDriver::getDEC(){
    return m_dec;
}

bool GPDXDriver::abort(){

    if(!sendCommand(CMD_SEND_STO))
        return false;

    return isOK();
}

bool GPDXDriver::setTrackEnabled(bool enabled){

    // change mode?
    char command[] = {CMD_START,PROTOCOL_S_TRM,CMD_SEP,'x',CMD_END};

    if(enabled){
        gpdxMode =  MODE_TRACKING;
        command[3]= '1';
    }
    else{
        gpdxMode = MODE_STOP;
        command[3]= '0';
    }

    LOGF_DEBUG("ReadScopeStatus : %d", gpdxMode);

    if(!sendCommand(command))
        return false;

    return isOK();
}

bool GPDXDriver::setTrackMode(uint8_t mode){

    char command[] = {CMD_START,PROTOCOL_S_TRR,CMD_SEP,'x',CMD_END};
    gpdxTrackmode = TR_SIDEREAL;
    command[3]=  '0';

    switch(mode){
    case 1:
        gpdxTrackmode = TR_SOLAR;
        command[3]=  '1';
        break;
    case 2:
        gpdxTrackmode = TR_LUNAR;
        command[3]=  '2';
        break;
    case 3:
        gpdxTrackmode = TR_CUSTOM;
        command[3]=  '3';
        break;
    }

    if(!sendCommand(command))
        return false;

    return isOK();
}

bool GPDXDriver::setTrackRate(double raRate, double deRate)
{

    char command[CMD_MAX_LEN]={0};
    command[0]=CMD_START; command[1]=PROTOCOL_S_TRV; command[2]=CMD_SEP;
    char body[CMD_MAX_LEN-3]={0};

    // step 0: convert double to char array
    snprintf(body, DRIVER_LEN, "%gX%gY", raRate, deRate);
    for(int i = 0; i<(CMD_MAX_LEN-3); i++)
        command[i+3]= body[i];

    // step 1: replace with protocoll commands
    replace(command,'X',CMD_INNER_SEP);
    replace(command,'Y',CMD_END);

    LOGF_DEBUG("setTackRate: %s",command);

    // step 2: send
    if(!sendCommand(command))
        return false;

    return isOK();
}

bool GPDXDriver::SetSlewRate(int index){

    LOGF_DEBUG("SetSlewRate called -  %d", index);
    char command[] = {CMD_START,PROTOCOL_S_SLE,CMD_SEP,'x',CMD_END};
    gpdxSlewrate = SR_GUIDE;
    command[3]=  '0';

    switch(index){
    case 1:
        gpdxSlewrate = SR_CENTERING;
        command[3]=  '1';
        break;
    case 2:
        gpdxSlewrate = SR_FIND;
        command[3]=  '2';
        break;
    case 3:
        gpdxSlewrate = SR_MAX;
        command[3]=  '3';
        break;
    }

    if(!sendCommand(command))
        return false;

    return isOK();
}

bool GPDXDriver::Sync(double RA, double DE)
{
    char command[CMD_MAX_LEN]={0};
    command[0]=CMD_START; command[1]=PROTOCOL_S_SRV; command[2]=CMD_SEP;
    char body[CMD_MAX_LEN-3]={0};

    // step 0: convert double to char array
    snprintf(body, DRIVER_LEN, "%gX%gY", RA, DE);
    for(int i = 0; i<(CMD_MAX_LEN-3); i++)
        command[i+3]= body[i];

    // step 1: replace with protocoll commands
    replace(command,'X',CMD_INNER_SEP);
    replace(command,'Y',CMD_END);

    LOGF_DEBUG("setSyncValues: %s",command);

    // step 2: send
    if(!sendCommand(command))
        return false;

    return isOK();
}
bool GPDXDriver::Goto(double RA, double DE)
{
    char command[CMD_MAX_LEN]={0};
    command[0]=CMD_START; command[1]=PROTOCOL_S_GTA; command[2]=CMD_SEP;
    char body[CMD_MAX_LEN-3]={0};

    // step 0: convert double to char array
    snprintf(body, DRIVER_LEN, "%gX%gY", RA, DE);
    for(int i = 0; i<(CMD_MAX_LEN-3); i++)
        command[i+3]= body[i];

    // step 1: replace with protocoll commands
    replace(command,'X',CMD_INNER_SEP);
    replace(command,'Y',CMD_END);

    LOGF_DEBUG("setGotoValues: %s",command);

    // step 2: send
    if(!sendCommand(command))
        return false;

    return isOK();
}
bool GPDXDriver::move()
{
    char command[CMD_MAX_LEN]={0};
    command[0]=CMD_START; command[1]=PROTOCOL_S_MOV;
    command[2]=CMD_SEP;  command[3]=CMD_END;
    sendCommand(command);
    return isOK();
}

bool GPDXDriver::MoveNS(GPDX_DIRECTION gpdxDir,bool start){

    LOGF_DEBUG("MoveNS gpdxDir %d",gpdxDir);
    LOGF_DEBUG("MoveNS start %d",start);

    char cNorth = (gpdxDir == GPDX_DIRECTION_NORTH) ? '1' : '2';
    char cStart = (start == true) ? '1' : '0';

    return true;
}
bool GPDXDriver::MoveWE(GPDX_DIRECTION gpdxDir,bool start){

    LOGF_DEBUG("MoveWE gpdxDir %d",gpdxDir);
    LOGF_DEBUG("MoveWE start %d",start);

    char cWest = (gpdxDir == GPDX_DIRECTION_WEST) ? '1' : '2';
    char cStart = (start == true) ? '1' : '0';

    return true;
}


bool GPDXDriver::sendCommand(const char * cmd)
{
    int nbytes_written = 0, nbytes_read = 0, rc = -1;
//    char cmd[CMD_MAX_LEN+2];
//    memcpy(cmd, cmd1, sizeof(cmd1));

    // len is determined by the end character

    int cmd_len = 0;
    bool found = false;
    for(int i = 0; i < (CMD_MAX_LEN); i++)
    {
        if( cmd[i] == CMD_END)
        {
            cmd_len = i+1;
            found = true;
            break;
        }
    }

    if(!found){

        LOGF_DEBUG("sendCommand Message was: <%s>",cmd);
        LOG_ERROR("sendCommand: WRONG FRORMAT - no end sign!");

        return false;
    }
    /* send message */

    tcflush(PortFD, TCIOFLUSH);

    if (cmd_len > 0)
    {
        LOGF_DEBUG("sendCommand Message ready to send: <%s>",cmd);
        LOGF_DEBUG("sendCommand Message length is: %d", cmd_len);

        rc = tty_write(PortFD, cmd, cmd_len, &nbytes_written);
    }
    else
    {
        LOGF_DEBUG("sendCommand CMD <%s>", cmd);
        rc = tty_write_string(PortFD, cmd, &nbytes_written);
    }

    if (rc != TTY_OK)
    {
        char errstr[MAXRBUF] = {0};
        tty_error_msg(rc, errstr, MAXRBUF);
        LOGF_ERROR("sendCommand Serial write error: %s.", errstr);
        return false;
    }


    /* read answer  */

    // clear old response message
    memset(response,0,sizeof(response));
    int res_len = -1; // using end char!

    if (response == nullptr){
        LOGF_ERROR("sendCommand response schould not be a null pointer!", ' ');
        return true;
    }


    if (res_len > 0)
        rc = tty_read(PortFD, response, res_len, DRIVER_TIMEOUT, &nbytes_read);
    else
        rc = tty_nread_section(PortFD, response, DRIVER_LEN, DRIVER_STOP_CHAR, DRIVER_TIMEOUT, &nbytes_read);

    if (rc != TTY_OK)
    {
        char errstr[MAXRBUF] = {0};
        tty_error_msg(rc, errstr, MAXRBUF);
        LOGF_ERROR("sendCommand Serial read error: %s.", errstr);
        return false;
    }

    if (res_len > 0)
    {
        char hex_res[DRIVER_LEN * 3] = {0};
        hexDump(hex_res, response, res_len);
        LOGF_DEBUG("sendCommand RES <%s>", hex_res);
    }
    else
    {
        LOGF_DEBUG("sendCommand RES <%s>", response);
    }

    tcflush(PortFD, TCIOFLUSH);

    return true;
}

void GPDXDriver::hexDump(char * buf, const char * data, int size)
{
    for (int i = 0; i < size; i++)
        sprintf(buf + 3 * i, "%02X ", static_cast<uint8_t>(data[i]));

    if (size > 0)
        buf[3 * size - 1] = '\0';
}

void GPDXDriver::replace( char* str, char old, char _new )
{
    while(*str)
    {
        if( *str == old ) *str = _new;
        str++;
    }
}
