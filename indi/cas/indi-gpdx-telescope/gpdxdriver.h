#ifndef INDI_GPDXDRIVER_H
#define INDI_GPDXDRIVER_H


#include "indicom.h"

#include "indiguiderinterface.h"
#include "inditelescope.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>

class GPDXDriver
{
public:
    GPDXDriver();

    void setSerialPort(int port){ if(PortFD == -1 )PortFD = port;}

    bool handshake();
    bool ReadScopeStatus();
    double getRA();
    double getDEC();

    bool abort();

    bool setTrackEnabled(bool enabled);
    bool setTrackMode(uint8_t mode);
    bool setTrackRate(double raRate, double deRate);
    bool SetSlewRate(int index);

    bool Sync(double RA, double DE);
    bool Goto(double RA, double DE);

    bool move();
    bool MoveNS(GPDX_DIRECTION gpdxDir,bool start);
    bool MoveWE(GPDX_DIRECTION gpdxDir,bool start);

    const char *getDeviceName();

public:  // ENUM STATUS FIELDS
    GPDX_MODE gpdxMode;
    GPDX_TRACKRATE gpdxTrackmode;
    GPDX_SLEWRATE gpdxSlewrate;

private :
    bool isOK();
    bool sendCommand(const char * cmd);
    void hexDump(char * buf, const char * data, int size);
    void replace( char* str, char old, char _new );

private :
    char response[CMD_MAX_LEN];  // after every read response contains the answer
    /////////////////////////////////////////////////////////////////////////////
    /// Static Helper Values
    /////////////////////////////////////////////////////////////////////////////
    int PortFD=-1;
    static const char DRIVER_START_CHAR { CMD_START };
    static const char DRIVER_STOP_CHAR { CMD_END };
    // Wait up to a maximum of 3 seconds for serial input
    static constexpr const uint8_t DRIVER_TIMEOUT {4};
    // Maximum buffer for sending/receving.
    static constexpr const uint8_t DRIVER_LEN {CMD_MAX_LEN};

    // read scope status
    double m_ra;
    double m_dec;
};

#endif // INDI_GPDXDRIVER_H
