#ifndef GPDX_H
#define GPDX_H

/**
  GENERAL HEADER FILE FOR TELESCOPE HANDLING
  @author: torsten.roehl@fsg-preetz.org
  @licence: GPL
*/

/* START: VERION ADJUST HERE */
#define VERSION_MAJOR   0
#define VERSION_MINOR   1
/* END: VERION ADJUST HERE */
#define CMD_MAX_LEN  31 // 30 + 1;
#define GPDX_DEVICENAME  "GPDX Great Polaris"

const char CMD_START = '[';
const char CMD_END   = ']';
const char CMD_SEP   = '|';
const char CMD_INNER_SEP = ':';
const int RA_LEN = 9;   //HH:MM:SS   (nullbyte)
const int DEC_LEN = 10; // sDD:MM:SS   (nullbyte)

#if defined(ARDUINO)
#include "Arduino.h"

#else
typedef unsigned char byte;
#endif

#if defined(ARDUINO)
byte lsCurrentRA[RA_LEN];
byte lsCurrentDEC[DEC_LEN];
byte lsTargetRA[RA_LEN];
byte lsTargetDEC[DEC_LEN];

double currentRA = 0;
double currentDE = 0;
double targetRA = 0;
double targetDE = 0;

static double trackRA = 0;
static double trackDE = 0;
static double slewRA = 0;
static double slewDE = 0;
#endif

//  avoid compiler warning, without eliminating warnings!
#ifdef __GNUC__
#define MAYBE_UNUSED __attribute__((used))
#else
#define MAYBE_UNUSED
#endif

/*************************************************************************************
     TYPEDEF
 ************************************************************************************/

/* MODE is allways one off the following */
typedef enum
{
  MODE_TRACKING,
  MODE_SLEWING,
  MODE_SYNCING,
  MODE_STOP,
  MODE_PARK,
  MODE_IDLE,

} GPDX_MODE;

typedef enum {
  GPDX_DIRECTION_NORTH,
  GPDX_DIRECTION_WEST,
  GPDX_DIRECTION_EAST,
  GPDX_DIRECTION_SOUTH

} GPDX_DIRECTION;

typedef enum
{
  TR_SIDEREAL,
  TR_SOLAR,
  TR_LUNAR,
  TR_CUSTOM
} GPDX_TRACKRATE;

typedef enum
{
  SR_GUIDE,
  SR_CENTERING,
  SR_FIND,
  SR_MAX
} GPDX_SLEWRATE;

/*********************
  COMMAND PROTOCOL
 *********************/

/*
   [CMD|DATA1|DATA2|DATAn]
   i)   3 byte header [CMD:
   ii)  1 byte command CMD
   iii) number of Datablocks depends on the CMD
   iv)  1 byte footer ]
*/

/** response **/
const byte OK = 49;
const byte ERROR = -1;
const byte ACK = 42;
/* getter */
const byte PROTOCOL_G_ACK = 'a';  // ACKnowledge Handshake RET=42  [A:]
const byte PROTOCOL_G_RA_ = 'b';  // Rektaszension FORMAT (8) HH:MM:SS RET=1 + 2 []
const byte PROTOCOL_G_DEC = 'c';  // DEClination FORMAT (9) sDD:MM:SS RET=1  + 2 []
const byte PROTOCOL_G_TRR = 'e';  // RET=TRackRate
const byte PROTOCOL_G_TRV = 'f';  // RET=[raValue:decValue]
const byte PROTOCOL_G_VER = 'g';  // RET=  [MAJOR:MINOR]
const byte PROTOCOL_G_STA = 'h';  // STAtus      RET=
const byte PROTOCOL_G_TAR = 'i';  // RET=  [targetRA:targetDEC]
/* setter */
const byte PROTOCOL_S_TRR = 'A';  // set TRackRate
const byte PROTOCOL_S_TRV = 'B';  // set Trackrate Custom Value
const byte PROTOCOL_S_SLE = 'C';  // set SLEvrate
const byte PROTOCOL_S_SRV = 'D';  // set Syncrate Custom Value
const byte PROTOCOL_S_STO = 'E';  // STOp all movements
const byte PROTOCOL_S_MOV = 'F';  // MOVe  Start slewing from current position to target position
const byte PROTOCOL_S_RA_ = 'G';  // Rektaszension FORMAT (8) HH:MM:SS
const byte PROTOCOL_S_DEC = 'H';  // DEClination FORMAT (9) sDD:MM:SS
const byte PROTOCOL_S_RST = 'I';  // ReSeT
const byte PROTOCOL_S_PAR = 'J';  // PARk Telescope
const byte PROTOCOL_S_UPA = 'K';  // UnPArk Telescope
const byte PROTOCOL_S_GRA = 'L';  // set RA Targetposition  RET=1
const byte PROTOCOL_S_GDE = 'M';  // set Dec Targetposition RET=1
const byte PROTOCOL_S_TRM = 'N';  // set gpdxMode
const byte PROTOCOL_S_GTA = 'O';  // set Goto RA DEC (target)
const byte PROTOCOL_S_MNS = 'P';  // moveNS
const byte PROTOCOL_S_MWE = 'Q';  // moveWE
/** sending/receiving commands **/
const char CMD_SEND_RECV_OK[]  = {CMD_START, OK, CMD_END};
const char CMD_SEND_ACK[] = {CMD_START, PROTOCOL_G_ACK, CMD_SEP, CMD_END};
const char CMD_RECV_ACK[] = {CMD_START, ACK, CMD_END};
const char CMD_SEND_VER[] = {CMD_START, PROTOCOL_G_VER, CMD_SEP, CMD_END};
const char CMD_RECV_VER[] = {CMD_START, VERSION_MAJOR, CMD_INNER_SEP, VERSION_MINOR, CMD_END};
const char CMD_SEND_STO[] = {CMD_START, PROTOCOL_S_STO, CMD_SEP, CMD_END};


#if defined(ARDUINO)

static GPDX_SLEWRATE gpdxSlewrate MAYBE_UNUSED;
static GPDX_TRACKRATE gpdxTrackrate MAYBE_UNUSED ;
static GPDX_MODE gpdxMode MAYBE_UNUSED;
static GPDX_DIRECTION gpdxDirection MAYBE_UNUSED;

/***************
     FUNCTIONS
 ****************/

static char asChar(byte value) {
  char res[2];
  itoa(value, res, 10);
  return res[0];
}

/*
   fill result with aa:bb:cc oder saa:bb:cc with s +/- using double value
*/
void asCharArray(double value, char* result, int res_len ) {



  int h = (int) value;
  double rest = (value - ((double) h)) * 3600; // remaining seconds
  rest = (rest < 0) ? -rest : rest;
  int m = (int) (rest / 60);
  rest = rest - (double)m * 60;
  int s = (int)rest;

  if (res_len == RA_LEN) // RA, always positive
    snprintf(result, res_len, "%02d:%02d:%02d", h, m, s);
  else { // DEC, positive or negative
    char sign = '+';
    if (h < 0) {
      h = -h;
      sign = '-';
    }
    snprintf(result, res_len, "%c%02d:%02d:%02d", sign, h, m, s);
  }
}


double asDoubleValue(double h, double m, double s) {
  double res = h;
  if (h < 0)
    res = res - (m / 60.0) - (s / 3600.0);
  else
    res = res + (m / 60.0) + (s / 3600.0);

  return res;
}

/*
   convert Xaa:bb:cc type of message to double
*/
double asDouble(char* msg, int msg_len) {

  String str = msg;
  int sign = (msg_len == RA_LEN) ? 1 : 0;

  double h = str.substring(0, 2 + sign).toDouble();
  double m = str.substring(3 + sign, 5 + sign).toDouble();
  double s = str.substring(6 + sign, 7 + sign).toDouble();

  return asDoubleValue(h, m, s);
}

/*
   convert message to double trackrate ra/de
*/
static bool trackValue(char* message) {

  String tmp = message;
  int index_1 = tmp.indexOf(CMD_SEP);
  int index_2 = tmp.indexOf(CMD_INNER_SEP);
  int index_3 = tmp.indexOf(CMD_END);

  if (index_1 == -1 || index_2 == -1 || index_3 == -1 )
    return false;

  // ra is between | and : (sep and innersep)
  String strRA = tmp.substring(index_1 + 1, index_2);
  trackRA = strRA.toDouble();

  // de is between : and ] (innersep and end)
  String strDE = tmp.substring(index_2 + 1, index_3 + 1);
  trackDE = strDE.toDouble();

  return true;
}

static bool syncValue(char* message) {

  String tmp = message;
  int index_1 = tmp.indexOf(CMD_SEP);
  int index_2 = tmp.indexOf(CMD_INNER_SEP);
  int index_3 = tmp.indexOf(CMD_END);

  if (index_1 == -1 || index_2 == -1 || index_3 == -1 )
    return false;

  // ra is between | and : (sep and innersep)
  String strRA = tmp.substring(index_1 + 1, index_2);
  currentRA  = strRA.toDouble();
  // de is between : and ] (innersep and end)
  String strDE = tmp.substring(index_2 + 1, index_3 + 1);
  currentDE = strDE.toDouble();
  // set also char array
  asCharArray(currentRA, lsCurrentRA, RA_LEN );
  asCharArray(currentDE, lsCurrentDEC, DEC_LEN );

  return true;
}

static bool setSlewRate(char rate) {

  gpdxSlewrate = SR_GUIDE;
  switch (rate) {
    case 1:
      gpdxSlewrate = SR_CENTERING;
      break;
    case 2:
      gpdxSlewrate = SR_FIND;
    case 3:
      gpdxSlewrate = SR_MAX;
      break;
  }
  return true;
}

static bool setTargetValue(char* message) {

  String tmp = message;
  int index_1 = tmp.indexOf(CMD_SEP);
  int index_2 = tmp.indexOf(CMD_INNER_SEP);
  int index_3 = tmp.indexOf(CMD_END);

  if (index_1 == -1 || index_2 == -1 || index_3 == -1 )
    return false;

  // ra is between | and : (sep and innersep)
  String strRA = tmp.substring(index_1 + 1, index_2);
  targetRA  = strRA.toDouble();

  // de is between : and ] (innersep and end)
  String strDE = tmp.substring(index_2 + 1, index_3 + 1);
  targetDE = strDE.toDouble();

  // set also char array
  asCharArray(targetRA, lsTargetRA, RA_LEN );
  asCharArray(targetDE, lsTargetDEC, DEC_LEN );

  return true;
}


/** alpha polaris ra/dec default values */
static  void defaultCoordinates() {

  //set lsCurrentRA = "(02:31:49)";
  currentRA =  2.53028;
  asCharArray(currentRA, lsCurrentRA, RA_LEN);

  // set currentDE = "(+89:15:50)";
  currentDE = 89.2639;
  asCharArray(currentDE, lsCurrentDEC, DEC_LEN);
}

static inline void initDriver() {
  defaultCoordinates();
  gpdxMode = MODE_STOP;
  gpdxTrackrate = TR_SIDEREAL;
  gpdxDirection = GPDX_DIRECTION_NORTH;

}

static inline void resetDriver() {
  initDriver();
}

/* send byte array */
static void send(byte lsData[], int len) {
  for (int i = 0; i < len; i++ ) {
    Serial.write(lsData[i]);
  }
}
/* send single byte */
static void send(byte data) {
  Serial.write(data);
}


/* read incomming whole message and return the protocol command for data handling */
static byte read() {

  int i = 0;
  char input[CMD_MAX_LEN];

  /* step 1: read  message  */

  if (Serial.available() > 0) {

    while (Serial.available() > 0) {
      input[i] = Serial.read();

      /* should not happend! */
      if (i == (CMD_MAX_LEN - 1)  )
        return ERROR;

      /* end of message ? */
      if ( input[i] == CMD_END)
        break;

      i++;
      delay(5);
    }

  }
  input[i + 1] = '\0'; // terminate with null byte


  // [CMD:datablock 0-n] little exception handling
  if (input[0] != CMD_START || input[2] != CMD_SEP || input[i] != CMD_END )
    return ERROR;

  /* step 2: handle message */

  char cmd = input[1]; // cmd is allways in position one!

  switch ( cmd ) {

    case PROTOCOL_S_STO: {
        gpdxMode = MODE_STOP;
        return PROTOCOL_S_STO;
      }

    case PROTOCOL_G_STA:
      return PROTOCOL_G_STA;

    case PROTOCOL_G_ACK:
      return PROTOCOL_G_ACK;

    case PROTOCOL_G_RA_:
      return PROTOCOL_G_RA_;

    case PROTOCOL_S_RA_:  {// HH:MM:SS

        if ( input[5] != CMD_INNER_SEP || input[8] != CMD_INNER_SEP )
          return ERROR;

        lsCurrentRA[0] = input[3];
        lsCurrentRA[1] = input[4];
        lsCurrentRA[2] = CMD_INNER_SEP;
        lsCurrentRA[3] = input[6];
        lsCurrentRA[4] = input[7];
        lsCurrentRA[5] = CMD_INNER_SEP;
        lsCurrentRA[6] = input[9];
        lsCurrentRA[7] = input[10];
        // skip nullbyte
        currentRA = asDouble(lsCurrentRA, RA_LEN);
        return PROTOCOL_S_RA_;
      }

    case PROTOCOL_G_DEC: // sDD:MM:SS
      return PROTOCOL_G_DEC;

    case PROTOCOL_S_DEC: {

        if ( input[6] != CMD_INNER_SEP || input[9] != CMD_INNER_SEP )
          return ERROR;

        lsCurrentDEC[0] = input[3];
        lsCurrentDEC[1] = input[4];
        lsCurrentDEC[2] = input[5];
        lsCurrentDEC[3] = CMD_INNER_SEP;
        lsCurrentDEC[4] = input[7];
        lsCurrentDEC[5] = input[8];
        lsCurrentDEC[6] = CMD_INNER_SEP;
        lsCurrentDEC[7] = input[10];
        lsCurrentDEC[8] = input[11];
        // skip nullbyte
        currentDE = asDouble(lsCurrentDEC, DEC_LEN);
        return PROTOCOL_S_DEC;
      }

    case PROTOCOL_S_GRA: {// HH:MM:SS

        if ( input[5] != CMD_INNER_SEP || input[8] != CMD_INNER_SEP )
          return ERROR;

        lsTargetRA[0] = input[3];
        lsTargetRA[1] = input[4];
        lsTargetRA[2] = CMD_INNER_SEP;
        lsTargetRA[3] = input[6];
        lsTargetRA[4] = input[7];
        lsTargetRA[5] = CMD_INNER_SEP;
        lsTargetRA[6] = input[9];
        lsTargetRA[7] = input[10];
        // skip nullbyte
        targetRA = asDouble(lsTargetRA, RA_LEN);

        return PROTOCOL_S_GRA;
      }

    case PROTOCOL_S_GDE: {// sDD:MM:SS

        if ( input[6] != CMD_INNER_SEP || input[9] != CMD_INNER_SEP )
          return ERROR;

        lsTargetDEC[0] = input[3];
        lsTargetDEC[1] = input[4];
        lsTargetDEC[2] = input[5];
        lsTargetDEC[3] = CMD_INNER_SEP;
        lsTargetDEC[4] = input[7];
        lsTargetDEC[5] = input[8];
        lsTargetDEC[6] = CMD_INNER_SEP;
        lsTargetDEC[7] = input[10];
        lsTargetDEC[8] = input[11];
        // skip nullbyte
        targetDE = asDouble(lsTargetDEC, DEC_LEN);
        return PROTOCOL_S_GDE;
      }

    case PROTOCOL_S_GTA: {
        if ( !setTargetValue(input))
          return ERROR;
        return PROTOCOL_S_GTA;
      }

    case PROTOCOL_G_TAR:
      return PROTOCOL_G_TAR;

    case PROTOCOL_S_MOV:
      return PROTOCOL_S_MOV;

    case PROTOCOL_S_TRR:
      gpdxTrackrate = input[3] - 48 ;
      return PROTOCOL_S_TRR;

    case PROTOCOL_G_TRR:
      return PROTOCOL_G_TRR;

    case PROTOCOL_S_TRV: {
        if ( !trackValue(input))
          return ERROR;
        return PROTOCOL_S_TRV;
      }

    case PROTOCOL_G_TRV:
      return PROTOCOL_G_TRV;

    case PROTOCOL_S_SLE: {
        setSlewRate(input[3]);

        return PROTOCOL_S_SLE;
      }

    case PROTOCOL_S_SRV: {
        if ( !syncValue(input))
          return ERROR;
        return PROTOCOL_S_SRV;
      }

    case PROTOCOL_S_TRM: {
        gpdxMode = MODE_STOP;
        if ( input[3] == '1' )
          gpdxMode = MODE_TRACKING;
        return PROTOCOL_S_TRM;
      }

    case PROTOCOL_S_RST:
      return PROTOCOL_S_RST;

    case PROTOCOL_G_VER:
      return PROTOCOL_G_VER;

  }

  return ERROR;
}
#endif // ARDUINO

#endif // GPDX_H

