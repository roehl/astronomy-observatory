/*
   Arduino - GPDX Treiber
*/

#include "driver_config.h"


void setup()
{
  Serial.begin(9600);
  initDriver();
}


void loop()
{

  byte cmd =  read();

  if (cmd == ERROR)
    return;

  switch (cmd) {

    case PROTOCOL_G_STA: {

        String strRA(currentRA, 6);
        String strDE(currentDE, 6);

        // [mode:ra:de]
        String strCMD = CMD_START + strRA + CMD_INNER_SEP + strDE + CMD_END;

        int len = strCMD.length() + 1;
        char command[len];
        strCMD.toCharArray(command, len );
        send(command, len);

        break;
      }

    case PROTOCOL_G_RA_: {

        char command[] = {CMD_START,
                          lsCurrentRA[0],
                          lsCurrentRA[1],
                          lsCurrentRA[2],
                          lsCurrentRA[3],
                          lsCurrentRA[4],
                          lsCurrentRA[5],
                          lsCurrentRA[6],
                          lsCurrentRA[7],
                          CMD_END
                         };
        send(command, 10);
        break;
      }

    case PROTOCOL_G_DEC: {

        char command[] = {CMD_START,
                          lsCurrentDEC[0],
                          lsCurrentDEC[1],
                          lsCurrentDEC[2],
                          lsCurrentDEC[3],
                          lsCurrentDEC[4],
                          lsCurrentDEC[5],
                          lsCurrentDEC[6],
                          lsCurrentDEC[7],
                          lsCurrentDEC[8],
                          CMD_END
                         };
        send(command, 11);
        break;
      }

    case PROTOCOL_S_GRA: // set goto ra
      send(CMD_SEND_RECV_OK, 3);
      break;

    case PROTOCOL_S_GDE:// set goto dec
      send(CMD_SEND_RECV_OK, 3);
      break;

    case PROTOCOL_S_RA_:
      send(CMD_SEND_RECV_OK, 3);
      break;

    case PROTOCOL_S_DEC:
      send(CMD_SEND_RECV_OK, 3);
      break;

    case PROTOCOL_G_TRR: {
        char tmp = asChar(gpdxTrackrate);
        char command[] = {CMD_START, tmp, CMD_END};
        send(command, 3);
        break;
      }

    case PROTOCOL_G_TAR: {
        String strRA(targetRA, 4);
        String strDE(targetDE, 4);
        String strCMD = CMD_START + strRA + CMD_INNER_SEP + strDE + CMD_END;
        int len = strCMD.length() + 1;
        char command[len];
        strCMD.toCharArray(command, len );
        send(command, len);
        break;
      }
    case PROTOCOL_S_TRM:
      send(CMD_SEND_RECV_OK, 3);
      break;

    case PROTOCOL_S_TRR:
      send(CMD_SEND_RECV_OK, 3);
      break;

    case PROTOCOL_S_TRV:
      send(CMD_SEND_RECV_OK, 3);
      break;

    case PROTOCOL_S_GTA:  {
        send(CMD_SEND_RECV_OK, 3);
        break;
      }

    case PROTOCOL_S_SLE: {
        send(CMD_SEND_RECV_OK, 3);
        break;
      }

    case PROTOCOL_G_TRV: {
        String strRA(trackRA, 4);
        String strDE(trackDE, 4);
        String strCMD = CMD_START + strRA + CMD_INNER_SEP + strDE + CMD_END;
        int len = strCMD.length() + 1;
        char command[len];
        strCMD.toCharArray(command, len );
        send(command, len);

        break;
      }

    case PROTOCOL_S_SRV:
      send(CMD_SEND_RECV_OK, 3);
      break;

    case PROTOCOL_S_STO:
      send(CMD_SEND_RECV_OK, 3);
      break;

    case PROTOCOL_S_MOV: {
      
        // TODO  move!
        currentRA =  targetRA;
        asCharArray(currentRA, lsCurrentRA, RA_LEN);
        currentDE = targetDE;
        asCharArray(currentDE, lsCurrentDEC, DEC_LEN);

        send(CMD_SEND_RECV_OK, 3);
        break;
      }

    case PROTOCOL_S_RST:
      resetDriver();
      send(CMD_SEND_RECV_OK, 3);
      break;

    case PROTOCOL_G_VER:
      send(CMD_RECV_VER, 5);
      break;

    case PROTOCOL_G_ACK:
      send(CMD_RECV_ACK, 3);
      break;

  }

  delay(5);
}




