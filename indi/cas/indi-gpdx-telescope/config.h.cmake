#ifndef CONFIG_H
#define CONFIG_H

/* Define INDI Data Dir */
#cmakedefine INDI_DATA_DIR "@INDI_DATA_DIR@"
/* Define Driver version */
#define GPDX_VERSION_MAJOR @GPDX_VERSION_MAJOR@
#define GPDX_VERSION_MINOR @GPDX_VERSION_MINOR@

#endif // CONFIG_H
