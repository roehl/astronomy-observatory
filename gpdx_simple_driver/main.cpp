#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include "gpdxdriver.h"

using namespace std;

/* einfaches programm um die klasse GPDXDriver die direkt mit d. arduino
kommuniziert zu testen */

int main(int argc, char** argv)
{
    // GPDXDriver gpdxDriver;
    char dev[] = {"/dev/ttyUSB0"};
    GPDXDriver gpdxDriver;
    if (!gpdxDriver.driverOpen(dev))
        cout << "error open" << endl;

    printf("\nBeenden mit X");

    int e=0;

    bool run = true;
    int w;
    int len;
    while(run)
    {

        /* read header 1 byte command */
        printf("\nBitte  Kommando eingeben: ");
        char  cmd[1];
        scanf("%s", cmd);

        /* check for exit code */
        if( cmd[0]  == 'X')
        {
            run = false;
            break;
        }

        /* read body must end with ] */
        printf("\nBitte  Body eingeben: ");
        char  body[21];
        scanf("%s", body);
        for(int i = 0; i< 21; i++)
        {
            if(body[i]== ']')
            {
                len = i+1;
                break;
            }
        }
        /* convert char to protocol byte command */
        byte bcmd = PROTOCOL_G_ACK;
        if(cmd[0] == 'B')
            bcmd = PROTOCOL_G_RA_;
        if(cmd[0] == 'C')
            bcmd = PROTOCOL_G_DEC;

        /* step 1: send */
        if(!gpdxDriver.sendMessage( bcmd,body))
            cout << "\nerror writeData \n";

        /* step 2: read answer */
        if(  !gpdxDriver.readMessage())
            cout << "\nerror readData - error code: " << e << endl;

        printf("Server Antwort:");

        /* loop through answer character array */
        for(int i =0; i< 21 ; i++)
        {
            printf("%c", gpdxDriver.getMessage()[i]);
        }
        printf("\n");



    }



    cout << "end" <<   endl;
    return 0;
}


