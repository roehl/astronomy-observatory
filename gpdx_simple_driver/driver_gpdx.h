#ifndef GPDX_H
#define GPDX_H

/**
  GENERAL HEADER FILE FOR TELESCOPE HANDLING
  @author: torsten.roehl@fsg-preetz.org
  @licence: GPL
*/


#if defined(ARDUINO)
  #include "Arduino.h"
#else
 typedef unsigned char byte;
#endif




#define VERSION   0x01

const int RA_LEN = 8;  //HH:MM:SS
const int DEC_LEN = 9;// sDD:MM:SS

static byte currentRA[RA_LEN];
static byte currentDEC[DEC_LEN];
static byte targetRA[RA_LEN];
static byte targetDEC[DEC_LEN];

/*********************
  COMMAND PROTOCOL
 *********************/

/*
   [CMD|DATA1|DATA2|DATAn]
   i)   3 byte header [CMD:
   ii)  1 byte command CMD
   iii) number of Datablocks depends on the CMD
   iv)  1 byte footer ]
*/

/* general */
#define CMD_MAX_LEN  26 // 25 + 1;
const char CMD_START = '[';
const char CMD_END   = ']';
const char CMD_SEP   = '|';
const char CMD_INNER_SEP = ':';

/** response **/
const byte OK = 1;
const byte ERROR = -1;
const byte ACK = 42;

/* getter */
const byte PROTOCOL_G_ACK = 65;  // ACKnowledge Handshake RET=42  [A:]
const byte PROTOCOL_G_RA_ = 66;  // Rektaszension FORMAT (8) HH:MM:SS RET=1
const byte PROTOCOL_G_DEC = 'C'; // DEClination FORMAT (9) sDD:MM:SS RET=1
const byte PROTOCOL_G_TRR = 68;  // RET=TRackRate
const byte PROTOCOL_G_VER = 69;  // RET=VERsion
const byte PROTOCOL_G_STA = 'F'; // STAtus      RET=
/* setter */
const byte PROTOCOL_S_TRR = 71;  // set TRackRate
const byte PROTOCOL_S_SLE = 72;  // set SLEvrate
const byte PROTOCOL_S_DIR = 'I'; // set DIRection   r R d D for motion
const byte PROTOCOL_S_STO = 74;  // STOp all movements
const byte PROTOCOL_S_MOV = 75;  // MOVe  Start slewing from current position to target position
const byte PROTOCOL_S_RA_ = 'L'; // Rektaszension FORMAT (8) HH:MM:SS
const byte PROTOCOL_S_DEC = 77;  // DEClination FORMAT (9) sDD:MM:SS
const byte PROTOCOL_S_RST = 78;  // ReSeT
const byte PROTOCOL_S_PAR = 79;  // PARk Telescope
const byte PROTOCOL_S_UPA = 'P'; // UnPArk Telescope
const byte PROTOCOL_S_GRA = 81;  // set RA Targetposition  RET=1
const byte PROTOCOL_S_GDE = 82;  // set Dec Targetposition RET=1



/***************
     TYPEDEF
 ****************/

/* MODE is allways one off the following */
typedef enum
{
  MODE_TRACKING = 1,
  MODE_SLEWING  = 2,
  MODE_STOP     = 3
} MODE;
static MODE Mode;

typedef enum {
  DIRECTION_NORTH = 1,
  DIRECTION_WEST = 2,
  DIRECTION_EAST = 3,
  DIRECTION_SOUTH = 4

} DIRECTION;
static DIRECTION Direction;

/* Different Trackrates */
typedef enum
{
  TR_SIDEREAL = 1,
  TR_SOLAR    = 2,
  TR_LUNAR    = 3,
  TR_CUSTOM   = 4
} TRACKRATE;
static TRACKRATE Trackrate;



#if defined(ARDUINO)




/***************
     FUNCTIONS
 ****************/




/** alpha polaris ra/dec default values */
static  void defaultCoordinates() {
  //set currentRA = "(02:31:50)";
  currentRA[0] = 48; currentRA[1] = 50; currentRA[2] = 58; currentRA[3] = 51;
  currentRA[4] = 49; currentRA[5] = 58; currentRA[6] = 53; currentRA[7] = 48; currentRA[8] = 35;
  // set currentDE = "(+89*15:00)";
  currentDEC[0] = 43;  currentDEC[1] = 56;  currentDEC[2] = 57;  currentDEC[3] = 42;  currentDEC[4] = 49;
  currentDEC[5] = 53;  currentDEC[6] = 58;  currentDEC[7] = 48;  currentDEC[8] = 48;  currentDEC[9] = 35;
}

static inline void initDriver() {
  defaultCoordinates();
  Mode = MODE_STOP;
  Trackrate = TR_SIDEREAL;
  Direction = DIRECTION_NORTH;

}

static inline void resetDriver() {
  initDriver();
}

/* send byte array */
static void send(byte lsData[], int len) {
  for (int i = 0; i < len; i++ ) {
    Serial.write(lsData[i]);
  }
}
/* send single byte */
static void send(byte data) {
  Serial.write(data);
}


/* read incomming whole message and return the protocol command for data handling */
static byte read() {

  int i = 0;
  char input[CMD_MAX_LEN];

  /* step 1: read  message  */

  if (Serial.available() > 0) {

    while (Serial.available() > 0) {
      input[i] = Serial.read();

      /* should not happend! */
      if (i == (CMD_MAX_LEN - 1)  )
        return ERROR;

      /* end of message ? */
      if ( input[i] == CMD_END)
        break;

      i++;
      delay(5);
    }

  }
  input[i + 1] = '\0'; // terminate with null byte


  // [CMD:datablock 0-n] little exception handling
  if (input[0] != CMD_START || input[2] != CMD_SEP || input[i] != CMD_END )
    return ERROR;

  /* step 2: handle message */

  char cmd = input[1]; // cmd is allways in position one!

  switch ( cmd ) {

    case PROTOCOL_S_STO:
      Mode = MODE_STOP;
      return PROTOCOL_S_STO;

    case PROTOCOL_G_ACK:
      return PROTOCOL_G_ACK;

    case PROTOCOL_G_RA_:
      return PROTOCOL_G_RA_;

    case PROTOCOL_S_RA_:  // HH:MM:SS

      if ( input[5] != CMD_INNER_SEP || input[8] != CMD_INNER_SEP )
        return ERROR;

      currentRA[0] = input[3];
      currentRA[1] = input[4];
      currentRA[2] = CMD_INNER_SEP;
      currentRA[3] = input[6];
      currentRA[4] = input[7];
      currentRA[5] = CMD_INNER_SEP;
      currentRA[6] = input[10];
      currentRA[7] = input[11];

      return PROTOCOL_S_RA_;

    case PROTOCOL_G_DEC: // sDD:MM:SS
      return PROTOCOL_G_DEC;

    case PROTOCOL_S_DEC:

      if ( input[6] != CMD_INNER_SEP || input[9] != CMD_INNER_SEP )
        return ERROR;

      currentDEC[0] = input[3];
      currentDEC[1] = input[4];
      currentDEC[2] = input[5];
      currentDEC[3] = CMD_INNER_SEP;
      currentDEC[4] = input[7];
      currentDEC[5] = input[8];
      currentDEC[6] = CMD_INNER_SEP;
      currentDEC[7] = input[10];
      currentDEC[8] = input[11];

      return PROTOCOL_S_DEC;

    case PROTOCOL_S_GRA: // HH:MM:SS

      if ( input[5] != CMD_INNER_SEP || input[8] != CMD_INNER_SEP )
        return ERROR;

      targetRA[0] = input[3];
      targetRA[1] = input[4];
      targetRA[2] = CMD_INNER_SEP;
      targetRA[3] = input[6];
      targetRA[4] = input[7];
      targetRA[5] = CMD_INNER_SEP;
      targetRA[6] = input[10];
      targetRA[7] = input[11];

      return PROTOCOL_S_GRA;

    case PROTOCOL_S_GDE: // sDD:MM:SS

      if ( input[6] != CMD_INNER_SEP || input[9] != CMD_INNER_SEP )
        return ERROR;

      targetDEC[0] = input[3];
      targetDEC[1] = input[4];
      targetDEC[2] = input[5];
      targetDEC[3] = CMD_INNER_SEP;
      targetDEC[4] = input[7];
      targetDEC[5] = input[8];
      targetDEC[6] = CMD_INNER_SEP;
      targetDEC[7] = input[10];
      targetDEC[8] = input[11];

      return PROTOCOL_S_GDE;

    case PROTOCOL_S_TRR:
      Trackrate = TR_LUNAR;
      return PROTOCOL_S_TRR;

    case PROTOCOL_G_TRR:
      return PROTOCOL_G_TRR;

    case PROTOCOL_G_VER:
      return PROTOCOL_G_VER;

    case PROTOCOL_G_STA:
      return PROTOCOL_G_STA;

  }

  return ERROR;
}
#endif // ARDUINO


#endif // GPDX_H
